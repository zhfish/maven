```
<repositories>
  <repository>
      <id>zhfish-maven-repo</id>
      <url>https://gitee.com/zhfish/maven/raw/master/repository</url>
  </repository>
</repositories>
```
```
<dependency>
    <groupId>net.zhfish</groupId>
    <artifactId>tio-spring-boot-starter</artifactId>
    <version>1.0.0</version>
</dependency>
```
```
tio:
  port: 9321
  handlerClass: net.zhfish.demo.webSocketHandler
```

https://git.oschina.net/tywo45/t-io

